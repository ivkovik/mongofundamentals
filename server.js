require("dotenv").config();
const http = require("http");
const { connectToDatabase } = require("./db/connection");
const app = require("./app");

const port = process.env.PORT || 8080;
const server = http.createServer(app);

connectToDatabase()
  .then((connection) => {
    // console.log(connection);
    server.listen(port, () => {
      console.log(`Server runining on port: ${port}`);
    });
  })
  .catch((err) => {
    console.log(err);
  });
