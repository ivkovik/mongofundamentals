const router = require("express").Router();
const {
  getAllParadoxes,
  editParadoxes,
  deleteParadoxes,
  addNewParadoxes,
} = require("../handlers/paradox");

router.get(`${process.env.API_PREFIX}/paradoxes`, getAllParadoxes);
router.post(`${process.env.API_PREFIX}/paradoxes`), addNewParadoxes;
router.put(`${process.env.API_PREFIX}/paradoxes/:id`, editParadoxes);
router.delete(`${process.env.API_PREFIX}/paradoxes/:id`, deleteParadoxes);

module.exports = router;
