const mongoose = require("mongoose");

const Paradox = mongoose.model("paradox", {
  name: String,
  description: String,
});

module.exports = Paradox;
