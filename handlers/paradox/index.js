const Paradox = require("../../core/paradox");

const getAllParadoxes = async (req, res, next) => {
  try {
    const allParadoxes = await Paradox.find();
    return res.status(200).json(allParadoxes);
  } catch (error) {
    return res.status(500).json(error);
  }
};
const addNewParadoxes = async (req, res, next) => {
  console.log(req);
  const { name, description } = req.body;

  if (!name || name.length < 6 || !description) {
    return res.status(400).json("invalid paradox data");
  }
  try {
    const newParadox = await Paradox.create({ name, description });
    return res.status(201).json(newParadox._id);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const editParadoxes = async (req, res, next) => {
  const paradoxId = req.params.id;

  const { name, description } = req.body;

  if (!paradoxId || !name || !description) {
    return res.status(400).json("Cannot update paradox");
  }

  try {
    await Paradox.findByIdAndUpdate(paradoxId, { name, description });
    return res.status(200).json(" updated paradox");
  } catch (error) {
    return res.status(500).json(error);
  }
};
const deleteParadoxes = async (req, res, next) => {
  const paradoxId = req.params.id;
  if (!paradoxId) {
    return res.status(400).json("Cannot find paradox");
  }
  try {
    await Paradox.deleteOne({ _id: paradoxId });
    res.status(200).json("Paradox was delited");
  } catch (error) {
    return res.status(500).json(error);
  }
};
module.exports = {
  getAllParadoxes,
  addNewParadoxes,
  editParadoxes,
  deleteParadoxes,
};
